//without the use of objects, our students from before would be organized as follows if we are to record additional information about them

// Spaghetti Code - when code is not organized enough, that it becomes hard to work with.
// Encapsulation - Organize related information(properties) and behavior(method) to belong to a single entity.

/*
	Encapsulate the following information into 4 student objects using object literals:
*/

/*
	Mini-Activity

	Update the logout and listGrades methods of student1 to be able to show student1's email with our message and the grades of student1.

*/

//create student one
/*
	let studentOneName = 'John';
	let studentOneEmail = 'john@mail.com';
	let studentOneGrades = [89, 84, 78, 88];

	//create student two
	let studentTwoName = 'Joe';
	let studentTwoEmail = 'joe@mail.com';
	let studentTwoGrades = [78, 82, 79, 85];

	//create student three
	let studentThreeName = 'Jane';
	let studentThreeEmail = 'jane@mail.com';
	let studentThreeGrades = [87, 89, 91, 93];

	//create student four
	let studentFourName = 'Jessie';
	let studentFourEmail = 'jessie@mail.com';
	let studentFourGrades = [91, 89, 92, 93];
*/

//actions that students may perform will be lumped together
function login(email){
    console.log(`${email} has logged in`);
}

function logout(email){
    console.log(`${email} has logged out`);
}

function listGrades(grades){
    grades.forEach(grade => {
        console.log(grade);
    })
}

//This way of organizing employees is not well organized at all.
//This will become unmanageable when we add more employees or functions
//To remedy this, we will create objects

// QUIZ
/*
	1. What is the term given to unorganized code that's very hard to work with?
		-Spaghetti Code

	2. How are object literals written in JS?
		-using curly braces or Object Literals '{}'

	3. What do you call the concept of organizing information and functionality to belong to an object?
		-Encapsulation

	4. If student1 has a method named enroll(), how would you invoke it?
		-student1.enroll()

	5. True or False: Objects can have objects as properties.
		-True

	6. What does the this keyword refer to if used in an arrow function method?
		-The window. Global.

	7. True or False: A method can have no parameters and still work.
		-True

	8. True or False: Arrays can have objects as elements.
		-True

	9. True or False: Arrays are objects.
		-True. Arrays are special forms of objects.

	10. True or False: Objects can have arrays as properties.
		-True
*/
// FUNCTION CODING
let student1 = {

	name: "John",
	email: "john@mail.com",
	grades: [89, 84, 78, 88],
	login: function() {

		// login should allow our student object to display its own email with our message.
		// this, when inside a method, refers to the object where it is in.
		// when this is inside a arrow function. 'this' wil lrefer to the global object. "Windows". 
		// console.log(this); // result: Window {window: Window, self: Window, document: document, name: '', location: Location, …}

	    console.log(`${this.email} has logged in`);

	},
	logout: function () {
	    console.log(`${this.email} has logged out`);
	},
	listGrades: function () {
	    this.grades.forEach(grade => {
	        console.log(grade);
	    });
	},
	computeAve: function() {
		const ave = (this.grades.reduce((a,b) => a + b))/(this.grades.length);
		return ave;
	},
	willPass: function() {
		const ave = Math.round(this.computeAve());

		if(ave >= 85){
			return true;
		} else {
			return false;
		};
	},
	willPassWithHonors: function() {
		const ave = Math.round(this.computeAve());

		if(ave >= 90){
			return true;
		} else if (ave >= 85 && ave < 90) {
			return false;
		} else {
			return undefined;
		};
	}
};


let student2 = {

	name: "Joe",
	email: "joe@mail.com",
	grades: [78, 82, 79, 85],
	login: function() {
		console.log(`${this.email} has logged in`);
	},
	logout: function () {
		console.log(`${this.email} has logged out`);
	},
	listGrades: function () {
		this.grades.forEach(grade => {
		   	console.log(grade);
		});
	},
	computeAve: function() {
		const ave = (this.grades.reduce((a,b) => a + b))/(this.grades.length);
		return ave;
	},
	willPass: function() {
		const ave = Math.round(this.computeAve());

		if(ave >= 85){
			return true;
		} else {
			return false;
		};
	},
	willPassWithHonors: function() {
		const ave = Math.round(this.computeAve());

		if(ave >= 90){
			return true;
		} else if (ave >= 85 && ave < 90) {
			return false;
		} else {
			return undefined;
		};
	}
};

let student3 = {

	name: "Jane",
	email: "jane@mail.com",
	grades: [87, 89, 91, 93],
	login: function() {
		console.log(`${this.email} has logged in`);
	},
	logout: function () {
		console.log(`${this.email} has logged out`);
	},
	listGrades: function () {
		this.grades.forEach(grade => {
		   	console.log(grade);
		});
	},
	computeAve: function() {
		const ave = (this.grades.reduce((a,b) => a + b))/(this.grades.length);
		return ave;
	},
	willPass: function() {
		const ave = Math.round(this.computeAve());

		if(ave >= 85){
			return true;
		} else {
			return false;
		};
	},
	willPassWithHonors: function() {
		const ave = Math.round(this.computeAve());

		if(ave >= 90){
			return true;
		} else if (ave >= 85 && ave < 90) {
			return false;
		} else {
			return undefined;
		};
	}
};

let student4 = {

	name: "Jessie",
	email: "jessie@mail.com",
	grades: [91, 89, 92, 93],
	login: function() {
		console.log(`${this.email} has logged in`);
	},
	logout: function () {
		console.log(`${this.email} has logged out`);
	},
	listGrades: function () {
		this.grades.forEach(grade => {
		   	console.log(grade);
		});
	},
	computeAve: function() {
		const ave = (this.grades.reduce((a,b) => a + b))/(this.grades.length);
		return ave;
	},
	willPass: function() {
		const ave = Math.round(this.computeAve());

		if(ave >= 85){
			return true;
		} else {
			return false;
		};
	},
	willPassWithHonors: function() {
		const ave = Math.round(this.computeAve());

		if(ave >= 90){
			return true;
		} else if (ave >= 85 && ave < 90) {
			return false;
		} else {
			return undefined;
		};
	}
};
